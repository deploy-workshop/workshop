# Deploy Workshop

## Подготовка к воркшопу
### Введение

Рассмотрим ситуацию: вы разработчик, у вас есть свой проект, который вы хотите разместить на хостинге.

Также вы хотите настроить Continuous Delivery - непрерывную доставку проекта до среды развертывания.
Это позволит вам производить развертывание проекта "по нажатию на кнопку".

На воркшопе мы пройдем этапы развертывания проекта с нуля: 
от создания сервера до настройки CI-CD. 

Нас ждут следующие шаги:
1. Создание VPS, на котором будем разворачивать проект; подключение к нему по SSH.
2. Ручное развертывание проекта на VPS; убедимся, что проект работает.
3. Автоматизация процесса развертывания:
    1. Настройка gitlab-ci.
    2. Создание gitlab runner (один на всех).
    3. Настройка docker-in-docker (для подключения к демону docker вашего VPS с раннера).
    4. Запук пайплайна развертывания!

### Подготовка

##### Для воркшопа понадобится:
1) Аккаунт на [gitlab](https://gitlab.com/) и установленный [клиент git](https://git-scm.com/downloads).
2) ssh-клиент (для windows - [PUTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html), в UNIX-системах это утилита `ssh`)
3) VPN для доступа к digitalocean ([Windscribe](https://goo.gl/CnPSsA) предлагает 2GB трафика бесплатно, нам хватит)

##### Последовательность шагов:
1. Убедиться, что все условия выше соблюдены.
2. Сгенерировать SSH ключ для доступа на сервер. Мне нужен ваш открытый ключ и его fingerprint. Выполните команды:
    ```bash
       # сгенерировать ключ (пропустить, если хотите использовать существующий ключ)
       ssh-keygen -t rsa

       # вывести открытый ключ в консоль
       cat ~/.ssh/<your_key_name>.pub
       
    ```

    Прислать **открытый** ключ (содержимое файла с расширением `*.pub`) и fingerprint ключа в Телеграм [@dpl_wrkshp](https://t.me/dpl_wrkshp).
    В сообщении используйте символ разметки: \`\`\`<ваш ключ>\`\`\`.

3. Дождитесь получения IP-адреса вашего сервера в чате.
4. Убедитесь, что вам удается подключиться по ssh:
    ```bash 
        ssh root@<ip вашего сервера>
        
        # на предложение добавить хост в список доверенных наберите
        > yes
        
        # если всё ОК - должна появиться консоль удаленного сервера
    ```
4. Сделайте форк проекта https://gitlab.com/deploy-workshop/project-backend.git 
5. Вы восхитительны! Ждем остальных участников и начинаем.

## Ручное развертывание

**Далее, мы будем работать на удаленном сервере по SSH.**

Чтобы сделать работу с инструкцией удобнее, создайте переменную окружения:
```bash
# адрес репозитория вашего проекта
export MY_PROJECT=https://gitlab.com/your_account/your_repo
```
Теперь во всех командах, где используется переменная `$MY_PROJECT`, вместо неё будет автоматически подставляться указанное вами значение.
Вам **не нужно** будет заменять `$MY_PROJECT` вручную - просто копировать команду как есть.

### Настройка окружения

Начните с установки docker и docker-compose:
```bash
# установка docker
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install -y docker.io
sudo systemctl start docker

# установка docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Склонируйте проект со своего репозитория на gitlab:
```bash
git clone $MY_PROJECT
cd project-backend
```

Соберите и запустите контейнеры при помощи docker-compose
```bash
docker network create project-net
docker-compose up -d --build
docker ps
```

Последняя команда выводит список контейнеров со статусом UP. Если сборка и запуск прошли успешно - должно быт три контейнера: project-backend_api, adminer и postgres. 

Ваш проект должен быть доступен через Интернет: 
попробуйте открыть в браузере ссылку `http://ваш_ip_адрес:8001`.

Если результат как на картинке - поздравляю, проект успешно развернут!
![](https://pp.userapi.com/c848532/v848532154/96d2e/PM1gaz7LQlU.jpg)

## Настройка автодеплоя 

На предыдущем шаге мы выполнили ручное развертывание проекта. 
Не очень удобно повторять это каждый раз, когда нужно задеплоить новые изменения, правда?

К счастью, в 2018 году есть множество инструментов, позволяющих автоматизировать данный процесс и интегрировать его с процессом разработки ПО. Мы будем использовать gitlab-ci.

### Настройка gitlab-ci

Воспользуемся заранее подготовленными файлами конфигурации `.gitlab-ci.yml` и `docker-compose-*.yml`, которые находятся в ветке `stage`.

Для этого создадим новую ветку `ci-demo` на основе ветки `stage` и выполним push в удаленный репозиторий:
```bash
git checkout -b stage --track origin/stage
git checkout -b ci-demo
git push --set-upstream origin ci-demo
```
Откройте вкладку `CI/CD -> Pipelines` в вашем репозитории. Вы увидите новый pipeline со статусом pending. 

Пайплайн должен содержать три стадии - build, test и deploy.

Чтобы pipeline поступил в работу, нужно настроить "работника" - Runner в терминологии gitlab. ([Гайд](https://docs.gitlab.com/runner/install/linux-manually.html) по запуску своего gitlab runner)

Подключитесь по ssh к серверу, который вы хотите использовать в качестве раннера, установите и зарегистрируйте gitlab runner (см. команды ниже).
> Если вы решили в качестве раннера использовать тот же сервер, что и для деплоя - docker и docker-compose уже должен быть установлен. 
```bash
# скачивание, установка (с созданием отдельного пользователя) и регистрация раннера
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner

# установка docker (пропустить, если уже установлен)
curl -sSL https://get.docker.com/ | sh

# установка docker-compose (пропустить, если уже установлен)
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# добавление пользователя для раннера, запуск и регистрация раннера
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo usermod -aG docker gitlab-runner

# наделение пользователя gitlab-runner правами на docker-директорию 
export USERR=gitlab-runner
sudo chown "$USERR":"$USERR" /home/"$USERR"/.docker -R
sudo chmod g+rwx "/home/$USERR/.docker" -R

sudo reboot

# подождать, пока перезагрузится, затем снова подключиться по ssh

sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
sudo gitlab-runner register
```

### Настройка docker-to-docker 
Чтобы раннер смог удаленно подключиться к docker-daemon, который запущен на вашем хосте - нужно настроить TLS сертификат для безопасного соединения.

Подключитесь по ssh к хосту, **на который будет производиться развертывание**.

Создайте переменную окружения, указав **ip-адрес gitlab-runner'а** вместо 0.0.0.0:
```bash
# ip-адрес вашего раннера
export RUNNER_IP=<ip раннера>
```

([Официальная инструкция](https://docs.docker.com/engine/security/https/#secure-by-default))
Выполните настройку TLS:
```bash
# создание папки .docker
mkdir ~/.docker && cd "$_"

# генерация открытого и закрытого ключа CA (Certificate Authority)
# попросит придумать и подтвердить passphrase для защиты ключа - можете ввести "1234", сейчас нам это не важно
openssl genrsa -aes256 -out ca-key.pem 4096

# далее везде, где будет требоваться ввод passphrase - вводите ту, которую вы указали на предыдущем шаге 

# создание запроса на подпись сертификата  
# попросит ввести контактную информацию - можете оставить все поля пустыми
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem

# генерация ключа сервера и запрос на подпись сертификата нашим CA
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=*" -sha256 -new -key server-key.pem -out server.csr

# CA подписывает публичный ключ сервера
echo subjectAltName = IP:$RUNNER_IP,IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf

# создание клиентского ключа и запрос на подпись 
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr

# подписание сервером клиентского сертификата
echo extendedKeyUsage = clientAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out cert.pem -extfile extfile.cnf

# перезапуск docker daemon с разрешением принимать подключения только от клиентов, имеющих сертификат, подписанный нашим CA
export DOCKER_TLS_VERIFY=1
sudo service docker stop
dockerd --tlsverify --tlscacert=ca.pem --tlscert=server-cert.pem --tlskey=server-key.pem -H=0.0.0.0:2376 -H unix:///var/run/docker.sock

# последняя команда запустит docker daemon, который захватит управление консолью 
```
Получившиеся файлы `{ca,cert,key}.pem` нужно добавить в директорию `~/.docker` раннера.
Скопируем их при помощи утилиты `scp`.

Для этого **со своего компьютера** выполните команды:  
```bash
# подставьте IP-адрес вашего сервера и сервера раннера
export SERVER_IP=<ip сервера>
export RUNNER_IP=<ip раннера>

# копируем с сервера на свой компьютер в папку keys_tmp
mkdir -p keys_tmp && scp root@$SERVER_IP:~/.docker/\{ca,cert,key\}.pem ./keys_tmp

# создадим папку для хранения ключей на раннере
ssh root@$RUNNER_IP "mkdir -p /home/gitlab-runner/.docker/$SERVER_IP"

# копируем в папку ~/.docker раннера 
scp ./keys_tmp/{ca,cert,key}.pem root@$RUNNER_IP:/home/gitlab-runner/.docker/$SERVER_IP
```

Чтобы runner смог подключаться к удаленному docker-серверу, нужно указать переменную окружения в `Settings -> CI/CD -> Secret variables`:
```
STAGING_HOST = <ip адрес сервера, на который производить деплой>
```

## Проверка автодеплоя

Пайплайн должен быть автоматически передан в работу, и все три стадии должны проходить успешно.

Если всё так - поздравляю! Вы успешно настроили автодеплой с использованием gitlab-ci.

Дальнейшие шаги:
- настроить docker-compose-stage для использования uwsgi + nginx в контейнерах
- настроить HTTPs 
- ...